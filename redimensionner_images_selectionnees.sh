#!/bin/bash
$fm_import    # import file manager variables

if ret=`zenity --scale --title='Quelle échelle?' --text='Spécifiez l’échelle de réduction (en %)' --value=50 2>/dev/null` # 50% par défaut
then
	# On créée le répertoire de destination s’il n’existe pas et on commence à réduire.
	if [ ! -d "resized" ]
	then
		mkdir resized
	fi
	
	let "count=0"
	for filename in "${fm_filenames[@]}"
	do
		zenity --info --title="Test" --text="Fichier $filename";
		convert "$filename" -resize "$ret"% resized/"$filename"; # filename entouré de quotes pour qu’Imagemagick ne recherche/créée pas plusieurs fichiers
		let "count = count + 1"
	done
	
	if [ $count -eq 1 ]
	then
		zenity --info --title="Fin de l’opération" --text="$count image a été redimensionnée à $ret % de sa taille d’origine";
	else
		zenity --info --title="Fin de l’opération" --text="$count images ont été redimensionnées à $ret % de leur taille d’origine";
	fi
	
fi

exit $?
