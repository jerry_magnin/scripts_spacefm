#!/bin/bash
$fm_import    # import file manager variables

for i in ${fm_files[@]}
do
	output=`echo $i | sed -re 's/\.pdf/_compressed.pdf/g'`
	# Available settings: /screen (76dpi), /ebook (150dpi), /printer (300dpi), /prepress (300dpi color preserving)
	gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -sOutputFile=$output $i
done

exit $?
